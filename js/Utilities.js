/**
 * This function changes the background color of the table.
 * @param {String} table_id - the id of the table generated.
 * @param {String} background_color - the background color input.
 */

function changeBackground(table_id, background_color) {
    let table = document.getElementById(table_id);
    table.style.backgroundColor = background_color;
}
/**
 * This function changes the border color of the td elements going one by one using a for loop (with indexes).
 * @param {String} table_id - the id of the table generated.
 * @param {String} border_color - the border color input.
 * @param {Integer} numberOfTDs - the number of columns
 * @param {Integers} numberOfTRs - the number of rows.
 */
function changeBorderColor(table_id, border_color, numberOfTDs, numberOfTRs) { 
    let table = document.getElementById(table_id);
    let tds = table.getElementsByTagName("td");
    for(let i = 0; i < (numberOfTRs * numberOfTDs); i++) {
        let td = tds[i];
        td.style.borderColor = " " + border_color;
    }
}
/**
 * This function changes the border width of the td elements going one by one using a for loop (with indexes).
 * @param {String} table_id - the id of the table generated.
 * @param {String} border_width - the border width input.
 * @param {Integer} numberOfTDs - the number of columns.
 * @param {Integer} numberOfTRs - the number of rows.
 */
function changeBorderWidth(table_id,border_width, numberOfTDs, numberOfTRs) { 
    let table = document.getElementById(table_id);
    let tds = table.getElementsByTagName("td");
    for(let i = 0; i < (numberOfTRs * numberOfTDs); i++) {
        let td = tds[i];
        td.style.borderWidth = ("" + border_width + "px");
    }
}
/**
 * This function changes the text color for the td elements.
 * @param {String} table_id - the id of the table generated.
 * @param {String} text_color - the text color input.
 */
function changeTextColor(table_id, text_color) {
    let table = document.getElementById(table_id);
    table.style.color = text_color;
}
/**
 * This function changes the width of the entire table.
 * @param {String} table_id - the id of the table generated.
 * @param {String} width_selected - the table width input.
 */
function changeTagWidth(table_id, width_selected) {
    let table = document.getElementById(table_id);
    table.style.width = width_selected;
}
/**
 * This function appends multiple strings and append them into one single object (using a nested for loop) 
 * called "code", which will will also be appended to its parent (textarea element).
 * @param {Integer} numberOfTRs 
 * @param {Integer} numberOfTDs 
 */
function codePrinter(numberOfTRs, numberOfTDs) {
    let code = "<table>\n";
        for(let i = 0; i < numberOfTRs; i++) {
                code += "   <tr>\n";
            for(let j = 0; j < numberOfTDs; j++) {
                code += "     <td>" + "cell "+ i + j + "</td>\n"
            }
            code += "   </tr>\n";
        }
    code += "</table>"
    let text_area = document.createElement("textarea");
    let parent = document.getElementById("table-html-space");
    text_area.textContent = code;
    parent.appendChild(text_area);
}