"uses strict";

/** The DomContentLoaded loads all the html first to later initialize the event listeners which which 
 * will call the function "table_generator".
 */

document.addEventListener('DOMContentLoaded', function() {

    let row_count = document.getElementById('row-count');
    row_count.addEventListener("blur", table_generator);

    let col_count = document.getElementById('col-count');
    col_count.addEventListener("blur", table_generator);

    let table_width = document.getElementById('table-width');
    table_width.addEventListener("blur", table_generator);

    let txt_color = document.getElementById('text-color');
    txt_color.addEventListener("blur", table_generator);

    let bcg_color = document.getElementById('background-color');
    bcg_color.addEventListener("blur", table_generator);

    let border_color = document.getElementById('border-color');
    border_color.addEventListener("blur", table_generator);

    let border_width = document.getElementById('border-width');
    border_width.addEventListener("blur", table_generator);

});

/**
 * The table_generator function builds a table based on the different inputs that the event listeners got triggered,
 * the table is built by using a nested for loop that will append a child to its parent. The 2 if-statements check
 * if either the table or the textarea object exists and it removes them if they do. The id for the table and tr serves
 * to specify which parent each td or tr must be appended to.
 */

    function table_generator() {

        if(document.getElementById("myTable_id") != null) {
            document.getElementById("myTable_id").remove();
        }

        let section_position = document.getElementById("table-render-space");
        let my_table = document.createElement("table");
        my_table.textContent = null;
        section_position.appendChild(my_table);

        let row_count = getRowCount();
        let col_count = getColCount();

        my_table.id = "myTable_id";
        my_table = document.getElementById("myTable_id");

        for(let i = 0; i < row_count; i++) {
            let tr_position = document.createElement("tr");
            tr_position.textContent = null;
            my_table.appendChild(tr_position);
                
            let tr_id = "tr_p" + i;
            tr_position.id = tr_id;

            tr_position = document.getElementById(tr_id);

            for(let j = 0; j < col_count; j++) {
                let td_position = document.createElement("td");
                td_position.textContent = ("cell " + i + j);
                tr_position.appendChild(td_position);
            }
        }

        changeBackground("myTable_id", getBackgroundColor());
        changeBorderColor("myTable_id", getBorderColor(),getColCount(),getRowCount());
        changeTextColor("myTable_id", getTextColor());
        changeTagWidth("myTable_id", `${getTableWidth()}%`);
        changeBorderWidth("myTable_id",getBorderWidth(),getColCount(),getRowCount());

        let x = document.querySelectorAll("textarea");

        codePrinter(row_count,col_count);

        if(x.length != 0) {
            document.querySelector("textarea").remove();
        }
    }
/**
 * Each of these getter methods return each of the inputs in the form with the objectif to be called in the 
 * table_generator function.
 */
    function getRowCount() {
        let row_count = document.getElementById('row-count').value;
        return row_count;
    }
    function getColCount() {
        let col_count = document.getElementById('col-count').value;
        return col_count;
    }
    function getBackgroundColor() {
        let bcg_color = document.getElementById('background-color').value;
        return bcg_color;
    }
    function getBorderColor() {
        let border_color = document.getElementById('border-color').value;
        return border_color;
    }
    function getBorderWidth() {
        let border_width = document.getElementById('border-width').value;
        return border_width;
    }
    function getTextColor() {
        let text_color = document.getElementById('text-color').value;
        return text_color;
    }
    function getTableWidth() {
        let table_width = document.getElementById('table-width').value;
        return table_width;
    }
   









